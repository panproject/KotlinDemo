package com.example.kotlindemo

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class CoroutinesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        verticalLayout {

            var tv = textView {
                gravity = Gravity.CENTER_HORIZONTAL
                textSize = 25f
                textColor = Color.BLUE
            }.lparams{
                topMargin=dip(100)
            }
            button("net require") {
                onClick {
                    requireNet(tv)
                    tv.text = "正在请求网络"
                }
            }


        }
    }

    private fun requireNet(textView: TextView) = GlobalScope.launch(Dispatchers.Main) {
        val content = fetchData()
        textView.text = content
    }

    suspend fun fetchData(): String {
        delay(3000)
        return "网络请求成功"
    }
}
