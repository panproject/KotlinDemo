package com.example.kotlindemo

import android.os.Bundle
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toast(intent.getStringExtra("from"))
        initView()
    }

    private fun initView() {
        verticalLayout {

            val etAccount = editText {
                hint = "input your account"
                lines = 1
            }

            val etPassword = editText {
                hint = "input your password"
                inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
                transformationMethod = PasswordTransformationMethod()
                lines = 1
            }

            button("login") {
                onClick {
                    requestLogin(etAccount.text.toString(), etPassword.text.toString())
                }
            }
        }
    }

    private fun requestLogin(account: String, password: String) {
        //模拟耗时网络请求
        toast("do login")
        doAsync {
            Thread.sleep(3000)
            uiThread {
                longToast("Login success account is $account  password is $password")
            }
        }


    }


}
