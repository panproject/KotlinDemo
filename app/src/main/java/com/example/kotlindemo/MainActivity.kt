package com.example.kotlindemo

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()

    }

    private fun initView() {
        verticalLayout {

            button("show dialog") {
                textSize = 13f
                textColor = Color.BLUE
                onClick {
                    alert("Content", "Title") {
                        customView {
                            editText {
                                hint = "this is hint"
                            }
                        }
                        positiveButton("Yes") { }
                        negativeButton("No") { }
                    }.show()
                }
            }.lparams(height = dip(50), width = matchParent) {
                horizontalMargin = dip(10)
                topMargin = dip(10)
            }


            button("Login Activity") {
                onClick {
                    startActivity<LoginActivity>("from" to "MainActivity")
                }
            }


            button("协程") {
                onClick {
                    startActivity<CoroutinesActivity>()
                }
            }

        }
    }
}
